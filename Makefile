OBJECTS = main.o nvtx.o Module.o pre_processing.o boundary_conditions.o 
MODULES = nvtx.mod Module.mod
# CASE = VST.f90
CASE = 2D_Riemann.f90


# FC= gfortran
# FFLAG = -O3
# FFLAG = -g -fbacktrace -ffpe-trap=zero,overflow,underflow
# FFLAG = -O2 -check bounds,uninit -g -fpe0 -traceback

FC= nvfortran
# FFLAG = -acc -fast -mcmodel=medium -ta=tesla:managed -Minfo=accel
FFLAG = -acc -fast#-Minfo=accel
# FFLAG = -fast -mcmodel=medium

# FC = ifort
# FFLAG = -xHost -fast

output: a.out

a.out: $(MODULES) $(OBJECTS) cases/$(CASE)
	$(FC) $(FFLAG) $(OBJECTS) cases/$(CASE) -o a.out

%.o: %.f90
	$(FC) $(FFLAG) -c $<

%.mod: %.f90
	$(FC) $(FFLAG) -c $<

.PHONY: clean
clean :
	rm -f *.o *.mod *.out
	
