
MODULE global_vars

	integer :: i,j,k,npr,ncr
	integer :: Nx, Ny, Nz, NGh, nprims, nconserv
	integer :: Nx_min,Nx_max,Ny_min,Ny_max,Nz_min,Nz_max
	
	! fluid/flow
	double precision :: Gamma, Pr, Re, Mach, T_ref
	
	! Timer
	double precision :: start_time, end_time, count_time
	
	! Cell point/averaged variables 
	double precision, allocatable :: conservative(:,:,:,:), primitive(:,:,:,:)
	double precision, allocatable :: cons_ini(:,:,:,:)
	double precision, allocatable :: Residual_RHS(:,:,:,:)
	double precision, allocatable :: C5_TBV(:,:,:,:), mp5TBV(:,:,:,:)
	
	! Variables on faces
	double precision, allocatable :: flux_ph(:,:,:,:)
	double precision, allocatable :: C5_phL(:,:,:,:), C5_phR(:,:,:,:)
	double precision, allocatable :: mp5phL(:,:,:,:), mp5phR(:,:,:,:)
	
	! grid variables
	double precision, allocatable :: X_CC(:), Y_CC(:), Z_CC(:)
	double precision :: dx,dy,dz
	
	! Temporal variables
	double precision :: time, time_step, t_end, CFL
	integer :: iter, rk_steps, step
	double precision, allocatable :: RK_factor1(:), RK_factor2(:), RK_factor3(:)
	
	! flags
	integer :: f2D, fViscous, fVisc_coeff, Write_freq, fDervsReady
	
END


MODULE viscous_variables




END


MODULE minmodfunctions

	contains
	
	double precision function MINMOD2(A,B)
		!$acc routine seq
		double precision, intent(in) :: A,B
		MINMOD2 = 0.5d0*(SIGN(1.d0,A)+SIGN(1.d0,B))*MIN(ABS(A),ABS(B))
	end function
	
	double precision function MINMOD4(W,A,B,C)
		!$acc routine seq
		double precision, intent(in) :: W,A,B,C
		MINMOD4 = 0.125d0*(SIGN(1.d0,W)+SIGN(1.d0,A))* &
				ABS((SIGN(1.d0,W)+SIGN(1.d0,B))*(SIGN(1.d0,W)+SIGN(1.d0,C)))* &
				MIN(ABS(W),ABS(A),ABS(B),ABS(C))
	end function

END


MODULE Interpolation_routines

	contains
	
	SUBROUTINE MP5_INTERPOLATE(PHI, PHI_iph, PHI_imh, nprims)
		
		!$acc routine seq
		
		! MP5 by Suresh and Hyunh Ref- https://doi.org/10.1006/JCPH.1997.5745
		use minmodfunctions
		implicit none
		
		Integer :: n_prim, nprims, i
		double precision, dimension(nprims)  :: PHI_iph, PHI_imh
		double precision, dimension(-2:3,nprims) :: PHI
		
		double precision :: PHI_mp, D_iph, D_imh, PHI_UL, PHI_AV, PHI_MD, PHI_LC, PHI_min, PHI_max
		double precision :: DI_minus, DI_zero, DI_plus
		double precision :: eps, alp, B2
		
		eps = 1d-40
		alp = 4.d0
		B2 = 4.d0/3.d0
		
		DO n_prim = 1,nprims
		
			i = 0
			
			PHI_iph(n_prim) = (2.d0*PHI(i-2,n_prim) - 13.d0*PHI(i-1,n_prim) &
				+ 47.d0*PHI(i,n_prim) + 27.d0*PHI(i+1,n_prim) - 3.d0*PHI(i+2,n_prim))/60.d0
					
			PHI_mp = PHI(i,n_prim) + MINMOD2(PHI(i+1,n_prim)-PHI(i,n_prim), alp*(PHI(i,n_prim)-PHI(i-1,n_prim)))
										
			IF ((PHI_iph(n_prim) - PHI(i,n_prim))*(PHI_iph(n_prim) - PHI_mp) >= eps) THEN
			
				Di_minus = PHI(i-2,n_prim) - 2.d0*PHI(i-1,n_prim) + PHI(i,n_prim)
				Di_zero	 = PHI(i-1,n_prim) - 2.d0*PHI(i,n_prim) + PHI(i+1,n_prim)
				DI_plus  = PHI(i,n_prim) - 2.d0*PHI(i+1,n_prim) + PHI(i+2,n_prim)
				
				D_iph = MINMOD4(4.d0*DI_zero - DI_plus , 4.d0*Di_plus - DI_zero, DI_zero, DI_plus)
				D_imh = MINMOD4(4.d0*Di_zero - DI_minus, 4.d0*Di_minus - DI_zero, DI_zero, DI_minus)
				
				PHI_UL = PHI(i,n_prim) + alp*(PHI(i,n_prim) - PHI(i-1,n_prim))
				PHI_AV = 0.5d0*(PHI(i,n_prim) + PHI(i+1,n_prim))
				PHI_MD = PHI_AV - 0.5d0*D_iph
				PHI_LC = PHI(i,n_prim) + 0.5d0*(PHI(i,n_prim)-PHI(i-1,n_prim)) + B2*D_imh
				
				PHI_min = MAX(MIN(PHI(i,n_prim), PHI(i+1,n_prim), PHI_MD), MIN(PHI(i,n_prim), PHI_UL, PHI_LC)) 
				PHI_max = MIN(MAX(PHI(i,n_prim), PHI(i+1,n_prim), PHI_MD), MAX(PHI(i,n_prim), PHI_UL, PHI_LC))
				
				PHI_iph(n_prim) = PHI_iph(n_prim) + MINMOD2(PHI_min-PHI_iph(n_prim), PHI_max-PHI_iph(n_prim))
	
			ENDIF
			
		ENDDO
		
		!======================================================================================================
		
		DO n_prim = 1,nprims
		
			i = 1
		
			PHI_imh(n_prim) = (2.d0*PHI(i+2,n_prim) - 13.d0*PHI(i+1,n_prim) &
				+ 47.d0*PHI(i,n_prim) + 27.d0*PHI(i-1,n_prim) - 3.d0*PHI(i-2,n_prim))/60.d0
			
			PHI_mp = PHI(i,n_prim) + MINMOD2(PHI(i-1,n_prim) - PHI(i,n_prim), alp*(PHI(i,n_prim) - PHI(i+1,n_prim)))
					
			IF ((PHI_imh(n_prim) - PHI(i,n_prim))*(PHI_imh(n_prim) - PHI_mp) >= eps) THEN
			
				DI_plus = PHI(i+2,n_prim) - 2.d0*PHI(i+1,n_prim) + PHI(i,n_prim)
				DI_zero	 = PHI(i+1,n_prim) - 2.d0*PHI(i,n_prim) + PHI(i-1,n_prim)
				DI_minus  = PHI(i,n_prim) - 2.d0*PHI(i-1,n_prim) + PHI(i-2,n_prim)
				
				D_iph = MINMOD4(4.d0*DI_zero - DI_plus, 4.d0*Di_plus - DI_zero, DI_zero, DI_plus)
				D_imh = MINMOD4(4.d0*DI_zero - DI_minus, 4.d0*Di_minus - DI_zero, DI_zero, DI_minus)
				
				PHI_UL = PHI(i,n_prim) + alp*(PHI(i,n_prim) - PHI(i+1,n_prim))
				PHI_AV = 0.5d0*(PHI(i,n_prim) + PHI(i-1,n_prim))
				PHI_MD = PHI_AV - 0.5d0*D_imh
				PHI_LC = PHI(i,n_prim) + 0.5d0*(PHI(i,n_prim)-PHI(i+1,n_prim)) + B2*D_iph
				
				PHI_min = MAX(MIN(PHI(i,n_prim), PHI(i-1,n_prim), PHI_MD), MIN(PHI(i,n_prim), PHI_UL, PHI_LC)) 
				PHI_max = MIN(MAX(PHI(i,n_prim), PHI(i-1,n_prim), PHI_MD), MAX(PHI(i,n_prim), PHI_UL, PHI_LC))
				
				PHI_imh(n_prim) = PHI_imh(n_prim) + MINMOD2(PHI_min-PHI_imh(n_prim), PHI_max-PHI_imh(n_prim))
			
			ENDIF
		
		ENDDO
		
	END SUBROUTINE
	
	
	SUBROUTINE WENO5_INTERPOLATE(PHI, PHI_iph, PHI_imh, nprims)
		
		!$acc routine seq
		! 5th Order WENO is
		! Ref(page-9)- https://www.dam.brown.edu/scicomp/old/reports/2001/BrownSC-2001-07.pdf
	
		implicit none
		
		Integer :: nprims
		double precision, dimension(-2:3,nprims) :: PHI
		double precision, dimension(nprims)  :: PHI_iph, PHI_imh
		double precision, dimension(3) :: PHI_hat	!Reconstructed var in each stencil
		double precision, dimension(3) :: w, Gma 	!non-linear and linear weights of each stencil
		double precision, dimension(3) :: B			!Smoothness indicator
		double precision, dimension(-2:2) :: PHI_L
		Integer :: n_prim
		double precision :: eps
		
		eps = 1.d-15
		
		Gma(1) = 1.d0/10.d0
		Gma(2) = 3.d0/5.d0
		Gma(3) = 3.d0/10.d0
		
		
		DO n_prim = 1,nprims
			
			!$ Cell Right face
			PHI_L = PHI(-2:2,n_prim)
			
			! Polynomials
			PHI_hat(1) = (2.d0*PHI_L(-2) -7.d0*PHI_L(-1) +11.d0*PHI_L(0))/6.d0
			PHI_hat(2) = (    -PHI_L(-1) +5.d0*PHI_L(0)  + 2.d0*PHI_L(1))/6.d0
			PHI_hat(3) = (2.d0*PHI_L( 0) +5.d0*PHI_L(1)  -      PHI_L(2))/6.d0
			
			! Smoothness indicators
			B(1) = 13.d0/12.d0*(PHI_L(-2) -2.d0*PHI_L(-1) +PHI_L(0))**2 + 0.25d0*(     PHI_L(-2) -4.d0*PHI_L(-1) +3.d0*PHI_L(0))**2
			B(2) = 13.d0/12.d0*(PHI_L(-1) -2.d0*PHI_L( 0) +PHI_L(1))**2 + 0.25d0*(     PHI_L(-1) -     PHI_L(1))**2
			B(3) = 13.d0/12.d0*(PHI_L( 0) -2.d0*PHI_L( 1) +PHI_L(2))**2 + 0.25d0*(3.d0*PHI_L(0)  -4.d0*PHI_L(1) +PHI_L(2))**2
			
			! Non-linear weights
			w(1) = Gma(1)/(eps+B(1))**2
			w(2) = Gma(2)/(eps+B(2))**2
			w(3) = Gma(3)/(eps+B(3))**2
			
			PHI_iph(n_prim) = SUM(w*PHI_hat)/SUM(w)
			
			!==============================================================================================
			!$ Next cell Left face
			PHI_L = PHI(-1:3,n_prim)
			
			PHI_hat(1) = (2.d0*PHI_L(2) -7.d0*PHI_L( 1) +11.d0*PHI_L( 0))/6.d0
			PHI_hat(2) = (    -PHI_L(1) +5.d0*PHI_L( 0) + 2.d0*PHI_L(-1))/6.d0
			PHI_hat(3) = (2.d0*PHI_L(0) +5.d0*PHI_L(-1) -      PHI_L(-2))/6.d0
			
			! Smoothness indicators
			B(1) = 13.d0/12.d0*(PHI_L(2) -2.d0*PHI_L( 1) +PHI_L( 0))**2 + 0.25d0*(PHI_L(2) -4.d0*PHI_L(1) +3.d0*PHI_L(0))**2
			B(2) = 13.d0/12.d0*(PHI_L(1) -2.d0*PHI_L( 0) +PHI_L(-1))**2 + 0.25d0*(PHI_L(1) - PHI_L(-1))**2
			B(3) = 13.d0/12.d0*(PHI_L(0) -2.d0*PHI_L(-1) +PHI_L(-2))**2 + 0.25d0*(3.d0*PHI_L(0) -4.d0*PHI_L(-1) +PHI_L(-2))**2
			
			! Non-linear weights
			w(1) = Gma(1)/(eps+B(1))**2
			w(2) = Gma(2)/(eps+B(2))**2
			w(3) = Gma(3)/(eps+B(3))**2
			
			PHI_imh(n_prim) = SUM(w*PHI_hat)/SUM(w)
			
		ENDDO
	
	END SUBROUTINE


END


MODULE Riemann_solvers

	contains
	
	SUBROUTINE HLLC(Rho_L,Ux_L,Uy_L,Uz_L,P_L,&
				   Rho_R,Ux_R,Uy_R,Uz_R,P_R,&
				   nx,ny,nz,&
				   gamma,f2D,&
				   Flux1,Flux2,Flux3,Flux4,Flux5)
		
		!$acc routine seq
		
		implicit none

		! Input variables
		double precision, intent(IN) :: Rho_L, Ux_L, Uy_L, Uz_L, P_L
		double precision, intent(IN) :: Rho_R, Ux_R, Uy_R, Uz_R, P_R
		double precision, intent(IN) :: nx, ny, nz
		double precision, intent(INOUT) :: Flux1, Flux2, Flux3, Flux4, Flux5
		double precision, intent(IN) :: gamma
		integer, intent(IN) :: f2D
		
		! Local variables
		double precision :: E_L, E_R
		double precision :: U_contra_L, U_contra_R, S_L, S_R, S_star
		double precision :: var1, var2, var3, var4, var5
		double precision :: sqrt_rho_L, sqrt_rho_R, divisor, u_average, v_average, w_average
		double precision :: h_ave_minus_uvw_ave,ucontra_ave, C_average
		
		
		E_L = P_L/(Gamma-1.d0) + Rho_L*(Ux_L**2+Uy_L**2+Uz_L**2)/2.d0
		E_R = P_R/(Gamma-1.d0) + Rho_R*(Ux_R**2+Uy_R**2+Uz_R**2)/2.d0
		U_contra_L = Ux_L*nx + Uy_L*ny + Uz_L*nz
		U_contra_R = Ux_R*nx + Uy_R*ny + Uz_R*nz
		
		! Roe averaging
		sqrt_rho_L = sqrt(Rho_L)
		sqrt_rho_R = sqrt(Rho_R)
		divisor	   = 1.d0/(sqrt_rho_R+sqrt_rho_L)
		
		u_average 	= ((Ux_L*sqrt_rho_L) + (Ux_R*sqrt_rho_R))*divisor
		v_average 	= ((Uy_L*sqrt_rho_L) + (Uy_R*sqrt_rho_R))*divisor
		w_average 	= ((Uz_L*sqrt_rho_L) + (Uz_R*sqrt_rho_R))*divisor
		ucontra_ave	= u_average*nx + v_average*ny + w_average*nz
		h_ave_minus_uvw_ave = (((E_L + P_L)/Rho_L*sqrt_rho_L) + ((E_R + P_R)/Rho_R*sqrt_rho_R))*divisor &
							  - 0.5d0 * (u_average**2.d0 + v_average**2.d0 + w_average**2.d0)
		C_average 	= sqrt((gamma-1.d0)*h_ave_minus_uvw_ave)
		
		
		! Wave speeds
		S_L = MIN(U_contra_L - Sqrt(Gamma*P_L/Rho_L), ucontra_ave - C_average)
		S_R = MAX(U_contra_R + Sqrt(Gamma*P_R/Rho_R), ucontra_ave + C_average)
		
		S_star = (Rho_R*U_contra_R*(S_R-U_contra_R) - Rho_L*U_contra_L*(S_L-U_contra_L) &
				+ (P_L-P_R)) &
				/(Rho_R*(S_R-U_contra_R) - Rho_L*(S_L-U_contra_L))
		
		
		IF (S_L>=0.d0)THEN
		
			Flux1 =  (rho_L*U_contra_L)
			Flux2 =  (rho_L*U_contra_L*Ux_L + P_L*nx)
			Flux3 =  (rho_L*U_contra_L*Uy_L + P_L*ny)
			Flux4 = ((rho_L*U_contra_L*Uz_L + P_L*nz))*(1-f2D)
			Flux5 =  (E_L + P_L) * U_contra_L
			
		ELSEIF(S_L<=0.d0 .and. S_star>=0.d0) THEN
		
			!Step3: Estimate fluxes
			Flux1 =  (rho_L*U_contra_L)
			Flux2 =  (rho_L*U_contra_L*Ux_L + P_L*nx)
			Flux3 =  (rho_L*U_contra_L*Uy_L + P_L*ny)
			Flux4 = ((rho_L*U_contra_L*Uz_L + P_L*nz))*(1-f2D)
			Flux5 =  (E_L + P_L) * U_contra_L
			
			var1 = 1.d0
			
			var2 = (S_star*nx + Ux_L*(ny**2+nz**2) &
					- Uy_L*nx*ny- Uz_L*nx*nz)
						
			var3 = (S_star*ny - Ux_L*(nx*ny) &
					+ Uy_L*(nx**2+nz**2) - Uz_L*(ny*nz))
						
			var4 = (S_star*nz - Ux_L*(nx*nz) &
					- Uy_L*(ny*nz) + Uz_L*(nx**2+ny**2))
						
			var5 = E_L/Rho_L+ (S_star - U_contra_L)*(S_star+P_L/(Rho_L*(S_L - U_contra_L)))
			
			Flux1 =  Flux1 + S_L*(Rho_L*(S_L - U_contra_L)/(S_L - S_star) *var1 - Rho_L)
			Flux2 =  Flux2 + S_L*(Rho_L*(S_L - U_contra_L)/(S_L - S_star) *var2 - Rho_L*Ux_L)
			Flux3 =  Flux3 + S_L*(Rho_L*(S_L - U_contra_L)/(S_L - S_star) *var3 - Rho_L*Uy_L)
			Flux4 = (Flux4 + S_L*(Rho_L*(S_L - U_contra_L)/(S_L - S_star) *var4 - Rho_L*Uz_L))*(1-f2D)
			Flux5 =  Flux5 + S_L*(Rho_L*(S_L - U_contra_L)/(S_L - S_star) *var5 - E_L)
			
						
		ELSEIF(S_star<=0.d0 .and. S_R>=0.d0) THEN
		
			Flux1 = (rho_R*U_contra_R)
			Flux2 = (rho_R*U_contra_R*Ux_R + P_R*nx)
			Flux3 = (rho_R*U_contra_R*Uy_R + P_R*ny)
			Flux4 = ((rho_R*U_contra_R*Uz_R + P_R*nz))*(1-f2D)
			Flux5 = (E_R + P_R) * U_contra_R
			
			var1 = 1.d0
			
			var2 = (S_star*nx + Ux_R*(ny**2+nz**2) &
						- Uy_R*nx*ny- Uz_R*nx*nz)
							
			var3 = (S_star*ny - Ux_R*(nx*ny) &
						+ Uy_R*(nx**2+nz**2)- Uz_R*(ny*nz))
							
			var4 = (S_star*nz - Ux_R*(nx*nz) &
						- Uy_R*(ny*nz) + Uz_R*(nx**2+ny**2))
							
			var5 = E_R/Rho_R+ (S_star - U_contra_R)*(S_star+P_R/(Rho_R*(S_R - U_contra_R)))
			
			Flux1 =  Flux1 + S_R*(Rho_R*(S_R - U_contra_R)/(S_R - S_star) *var1 - Rho_R)
			Flux2 =  Flux2 + S_R*(Rho_R*(S_R - U_contra_R)/(S_R - S_star) *var2 - Rho_R*Ux_R)
			Flux3 =  Flux3 + S_R*(Rho_R*(S_R - U_contra_R)/(S_R - S_star) *var3 - Rho_R*Uy_R)
			Flux4 = (Flux4 + S_R*(Rho_R*(S_R - U_contra_R)/(S_R - S_star) *var4 - Rho_R*Uz_R))*(1-f2D)
			Flux5 =  Flux5 + S_R*(Rho_R*(S_R - U_contra_R)/(S_R - S_star) *var5 - E_R)
			
								
		ELSEIF(S_R<= 0.d0) THEN
		
			Flux1 =  (rho_R*U_contra_R)
			Flux2 =  (rho_R*U_contra_R*Ux_R + P_R*nx)
			Flux3 =  (rho_R*U_contra_R*Uy_R + P_R*ny)
			Flux4 = ((rho_R*U_contra_R*Uz_R + P_R*nz))*(1-f2D)
			Flux5 =  (E_R + P_R) * U_contra_R
			
		ENDIF
	
	
	end subroutine
	
	
	SUBROUTINE HLL(Rho_L,Ux_L,Uy_L,Uz_L,P_L,&
				   Rho_R,Ux_R,Uy_R,Uz_R,P_R,&
				   Mx_phJ,My_phJ,Mz_phJ,&
				   gamma,f2D,&
				   Flux1,Flux2,Flux3,Flux4,Flux5)
		
		!$acc routine seq
		
		implicit none

		! Input variables
		double precision, intent(IN) :: Rho_L, Ux_L, Uy_L, Uz_L, P_L
		double precision, intent(IN) :: Rho_R, Ux_R, Uy_R, Uz_R, P_R
		double precision, intent(IN) :: Mx_phJ, My_phJ, Mz_phJ
		double precision, intent(INOUT) :: Flux1, Flux2, Flux3, Flux4, Flux5
		double precision, intent(IN) :: gamma
		integer, intent(IN) :: f2D
		
		! Local variables
		double precision :: E_L, E_R
		double precision :: U_contra_L, U_contra_R, S_L, S_R
		double precision :: var1, var2, var3, var4, var5
		double precision :: sqrt_rho_L, sqrt_rho_R, divisor, u_average, v_average, w_average
		double precision :: h_ave_minus_uvw_ave,ucontra_ave, C_average
		
		
		E_L = P_L/(Gamma-1.d0) + Rho_L*(Ux_L**2+Uy_L**2+Uz_L**2)/2.d0
		E_R = P_R/(Gamma-1.d0) + Rho_R*(Ux_R**2+Uy_R**2+Uz_R**2)/2.d0
		U_contra_L = Ux_L*Mx_phJ + Uy_L*My_phJ + Uz_L*Mz_phJ
		U_contra_R = Ux_R*Mx_phJ + Uy_R*My_phJ + Uz_R*Mz_phJ
		
		! Roe averaging
		sqrt_rho_L = sqrt(Rho_L)
		sqrt_rho_R = sqrt(Rho_R)
		divisor	   = 1.d0/(sqrt_rho_R+sqrt_rho_L)
		
		
		u_average 	= ((Ux_L*sqrt_rho_L) + (Ux_R*sqrt_rho_R))*divisor
		v_average 	= ((Uy_L*sqrt_rho_L) + (Uy_R*sqrt_rho_R))*divisor
		w_average 	= ((Uz_L*sqrt_rho_L) + (Uz_R*sqrt_rho_R))*divisor
		ucontra_ave	= u_average*Mx_phJ + v_average*My_phJ + w_average*Mz_phJ
		h_ave_minus_uvw_ave = (((E_L + P_L)/Rho_L*sqrt_rho_L) + ((E_R + P_R)/Rho_R*sqrt_rho_R))*divisor &
							  - 0.5d0 * (u_average**2.d0 + v_average**2.d0 + w_average**2.d0)
		C_average 	= sqrt((gamma-1.d0)*h_ave_minus_uvw_ave*(Mx_phJ**2+My_phJ**2+Mz_phJ**2))
		
		
		! Wave speeds
		S_L = MIN(U_contra_L - Sqrt(Gamma*P_L/Rho_L*(Mx_phJ**2+My_phJ**2+Mz_phJ**2)), ucontra_ave - C_average)
		S_R = MAX(U_contra_R + Sqrt(Gamma*P_R/Rho_R*(Mx_phJ**2+My_phJ**2+Mz_phJ**2)), ucontra_ave + C_average)
		
		IF (S_L>=0.d0) THEN
		
			Flux1 =  (rho_L*U_contra_L)
			Flux2 =  (rho_L*U_contra_L*Ux_L + P_L*Mx_phJ)
			Flux3 =  (rho_L*U_contra_L*Uy_L + P_L*My_phJ)
			Flux4 = ((rho_L*U_contra_L*Uz_L + P_L*Mz_phJ))*(1-f2D)
			Flux5 =  (E_L + P_L) * U_contra_L
			
		ELSEIF(S_L <= 0.d0 .and. S_R>=0.d0) THEN
		
			Flux1 = (S_R*(rho_L*U_contra_L)  - S_L*(rho_R*U_contra_R)  &
				   + S_L*S_R*(Rho_R - Rho_L))/(S_R - S_L)
			Flux2 = (S_R*(rho_L*U_contra_L*Ux_L + P_L*Mx_phJ) &
				   - S_L*(rho_R*U_contra_R*Ux_R + P_R*Mx_phJ) &
				   + S_L*S_R*(Rho_R*Ux_R - Rho_L*Ux_L))/(S_R - S_L)
			Flux3 = (S_R*(rho_L*U_contra_L*Uy_L + P_L*My_phJ) &
				   - S_L*(rho_R*U_contra_R*Uy_R + P_R*My_phJ) &
				   + S_L*S_R*(Rho_R*Uy_R - Rho_L*Uy_L))/(S_R - S_L)
			Flux4 =((S_R*((rho_L*U_contra_L*Uz_L + P_L*Mz_phJ) )*(1-f2D) &
				   - S_L*((rho_R*U_contra_R*Uz_R + P_R*Mz_phJ) )*(1-f2D) &
				   + S_L*S_R*(Rho_R*Uz_R - Rho_L*Uz_L))/(S_R - S_L))*(1-f2D)
			Flux5 = (S_R*(E_L + P_L) * U_contra_L  - S_L*(E_R + P_R) * U_contra_R &
				   + S_L*S_R*(E_R - E_L))/(S_R - S_L)
			
		ELSEIF(S_R<=0.d0) THEN
		
			Flux1 =  (rho_R*U_contra_R)
			Flux2 =  (rho_R*U_contra_R*Ux_R + P_R*Mx_phJ)
			Flux3 =  (rho_R*U_contra_R*Uy_R + P_R*My_phJ)
			Flux4 = ((rho_R*U_contra_R*Uz_R + P_R*Mz_phJ))*(1-f2D)
			Flux5 =  (E_R + P_R) * U_contra_R
			
		ENDIF
	
	
	end subroutine
	
	
	SUBROUTINE LaxF(Rho_L,Ux_L,Uy_L,Uz_L,P_L,&
				    Rho_R,Ux_R,Uy_R,Uz_R,P_R,&
				    Mx_phJ,My_phJ,Mz_phJ,&
				    gamma,f2D,&
				    Flux1,Flux2,Flux3,Flux4,Flux5)
		
		!$acc routine seq
		
		implicit none

		! Input variables
		double precision, intent(IN) :: Rho_L, Ux_L, Uy_L, Uz_L, P_L
		double precision, intent(IN) :: Rho_R, Ux_R, Uy_R, Uz_R, P_R
		double precision, intent(IN) :: Mx_phJ, My_phJ, Mz_phJ
		double precision, intent(INOUT) :: Flux1, Flux2, Flux3, Flux4, Flux5
		double precision, intent(IN) :: gamma
		integer, intent(IN) :: f2D
		
		! Local variables
		double precision :: E_L, E_R
		double precision :: U_contra_L, U_contra_R, S
		double precision :: sqrt_rho_L, sqrt_rho_R, divisor, u_average, v_average, w_average
		double precision :: h_ave_minus_uvw_ave,ucontra_ave, C_average
		
		
		E_L = P_L/(Gamma-1.d0) + Rho_L*(Ux_L**2+Uy_L**2+Uz_L**2)/2.d0
		E_R = P_R/(Gamma-1.d0) + Rho_R*(Ux_R**2+Uy_R**2+Uz_R**2)/2.d0
		U_contra_L = Ux_L*Mx_phJ + Uy_L*My_phJ + Uz_L*Mz_phJ
		U_contra_R = Ux_R*Mx_phJ + Uy_R*My_phJ + Uz_R*Mz_phJ
		
		! Roe averaging
		sqrt_rho_L = sqrt(Rho_L)
		sqrt_rho_R = sqrt(Rho_R)
		divisor	   = 1.d0/(sqrt_rho_R+sqrt_rho_L)
		
		
		u_average 	= ((Ux_L*sqrt_rho_L) + (Ux_R*sqrt_rho_R))*divisor
		v_average 	= ((Uy_L*sqrt_rho_L) + (Uy_R*sqrt_rho_R))*divisor
		w_average 	= ((Uz_L*sqrt_rho_L) + (Uz_R*sqrt_rho_R))*divisor
		ucontra_ave	= u_average*Mx_phJ + v_average*My_phJ + w_average*Mz_phJ
		h_ave_minus_uvw_ave = (((E_L + P_L)/Rho_L*sqrt_rho_L) + ((E_R + P_R)/Rho_R*sqrt_rho_R))*divisor &
							  - 0.5d0 * (u_average**2.d0 + v_average**2.d0 + w_average**2.d0)
		C_average 	= sqrt((gamma-1.d0)*h_ave_minus_uvw_ave*(Mx_phJ**2+My_phJ**2+Mz_phJ**2))
		
		S = ABS(ucontra_ave) + C_average
		
		!Step3: Estimate fluxes
		Flux1 =  (rho_L*U_contra_L)
		Flux2 =  (rho_L*U_contra_L*Ux_L + P_L*Mx_phJ)
		Flux3 =  (rho_L*U_contra_L*Uy_L + P_L*My_phJ)
		Flux4 = ((rho_L*U_contra_L*Uz_L + P_L*Mz_phJ))*(1-f2D)
		Flux5 =  (E_L + P_L) * U_contra_L
		
		Flux1 = Flux1 +  (rho_R*U_contra_R)
		Flux2 = Flux2 +  (rho_R*U_contra_R*Ux_R + P_R*Mx_phJ)
		Flux3 = Flux3 +  (rho_R*U_contra_R*Uy_R + P_R*My_phJ)
		Flux4 = Flux4 + ((rho_R*U_contra_R*Uz_R + P_R*Mz_phJ))*(1-f2D)
		Flux5 = Flux5 +  (E_R + P_R) * U_contra_R
		
		
		Flux1 =  0.5d0*(Flux1 - S*4*(Rho_R - Rho_L))
		Flux2 =  0.5d0*(Flux2 - S*4*(Rho_R*Ux_R - Rho_L*Ux_L))
		Flux3 =  0.5d0*(Flux3 - S*4*(Rho_R*Uy_R - Rho_L*Uy_L))
		Flux4 = (0.5d0*(Flux4 - S*4*(Rho_R*Uz_R - Rho_L*Uz_L)))*(1-f2D)
		Flux5 =  0.5d0*(Flux5 - S*4*(E_R - E_L))
	
	
	end subroutine
	

END
